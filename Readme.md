
# Overview of Sealing Practices at Kültepe during the Anatolian−Old Assyrian Trade Network Period (source and data)

This repository contains the manuscript version of the following paper.

Strupler, Néhémie Overview of Sealing Practices at Kültepe during the
Anatolian−Old Assyrian Trade Network Period, *in:* Fikri Kulakoğlu,
Guido Kryszat and Cécile Michel. *Cultural Exchange and Current Research
in Kültepe and its Surroundings*, Kültepe International Meetings 4, Brepols Publishers, pp.181-196, 2021

> *Keywords*: Glyptic, Blockchain, Ownership, Exchange, Infrastructure,
> Trading, Network, Turkey, Anatolia, Archaeology, Bronze Age, Old
> Assyrian

All images used in the paper can be found in the [img](./img) directory.

The main to create a pdf, docx, epub or html page is recorded in the
[Makefile](./Makefile) file and specific configuration files are stored
in the [config](./config) directory. The [bib.bib](./bib.bib) file
contains the reference database. Finaly, the file
[.gitlab-ci.yml](/.gitlab-ci.yml) configure specific instructions for GitLab CI/CD
to render the pdf and generate the webpage


# Licence
[![CC BY-SA](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg)](200~https://creativecommons.org/licenses/by-sa/4.0/)

# Link to Manuscript

[HAL: hal-03440315](https://hal.archives-ouvertes.fr/hal-03440315)

[![HAL 03440315](https://halshs.archives-ouvertes.fr/public/halshs.png)](https://hal.archives-ouvertes.fr/hal-03440315)

