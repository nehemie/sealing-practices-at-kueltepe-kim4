PAPER=Strupler_Manuscript_Kultepe_Seals_KIM4-10
PAPER_MD=$(PAPER).md
PAPER_HTML=$(PAPER).html
PAPER_DOCX=$(PAPER).docx
PAPER_PDF=$(PAPER).pdf
PAPER_EPUB=$(PAPER).epub
BIB=bib.bib

paper_html:
	pandoc $(PAPER_MD) \
		-f "markdown+implicit_figures" \
    --strip-comments \
		--self-contained \
		--standalone \
    --toc \
    --toc-depth=4\
		--metadata toc-title="Contents" \
    --bibliography=$(BIB) \
		--template="config/template.html"\
		-c "config/pandoc.css" \
		-t html \
	 	-o $(PAPER_HTML)

paper_pdf:
	pandoc $(PAPER_MD) \
		--from "markdown+raw_tex" \
		--metadata link-citations\
		--bibliography=$(BIB) \
		--include-in-header=config/latex_head.tex \
		--pdf-engine=xelatex \
		--output=$(PAPER_PDF)

paper_docx:
	pandoc $(PAPER_MD) \
    --bibliography=$(BIB) \
		-t docx \
		-o $(PAPER_DOCX)

paper_epub:
	pandoc $(PAPER_MD) \
    --bibliography=$(BIB) \
		-s \
		-o $(PAPER_EPUB)
