---
title: "Overview of Sealing Practices at Kültepe during the Anatolian−Old Assyrian Trade Network Period"
author: "Néhémie Strupler"
date: "Author's manuscript published in Kulakoğlu, F.; Kryszat, G. & Michel, C. (Eds.) *Cultural Exchange and Current Research in Kültepe and its Surroundings Kültepe*, Brepols Publishers, 2021, 181-196"
keywords: [seal, blockchain, ownership, exchange, infrastructure]
abstract: |

  [**p. 181 starts**]

  **Abstract:**

  Sealing is a well-known technique to mark ownership and identity on
  objects in order to protect their integrity.  It is essential to
  administrative activities taking place beyond the face-to-face sphere of
  interactions.

  During the Anatolian−Old Assyrian Trade Period seals were intensively
  used on different clay supports: on written objects such as tablets or
  envelopes or on clay lumps securing the opening of objects. This paper
  reviews sealing assemblages from Kültepe to show similarity and
  diversity in the production, use, and deposition of the sealings
  during the Anatolian−Old Assyrian Trade Period. It seeks to highlight
  their functions at Kültepe and to spotlight some connections with
  assemblages from other sites.

documentclass: scrartcl
header-includes: |
  \RedeclareSectionCommand[
    beforeskip=-10pt plus -2pt minus -1pt,
    afterskip=1sp plus -1sp minus 1sp,
    font=\normalfont\itshape]{paragraph}
  \RedeclareSectionCommand[
    beforeskip=-10pt plus -2pt minus -1pt,
    afterskip=1sp plus -1sp minus 1sp,
    font=\normalfont\scshape,
    indent=0pt]{subparagraph}
classoption:
 - twocolumn
#- landscape
geometry:
- top=20mm
- left=20mm
- right=20mm
- heightrounded
hyperrefoptions:
- linktoc=all
- pdfwindowui
- pdfpagemode=FullScreen
log-file: log.json
toc: true
toc-depth: 4
number-sections: true
citation_package: natbib

---

# Introduction


‘Sealing a document’ is a long-lasting practice, but as yet a field of
innovative application, as demonstrated for instance in the 2010s by the
eruption of the ‘blockchain’ technology, a new mechanism to ‘lock’
data. Functions and uses of seals today bear strong similarities with
those dating from four thousand years ago or more. A sealing serves two
main purposes: to protect the integrity of a commodity and/&nbsp;or
ascertain its ownership. Nowadays, (physical) seals are still used
everywhere, such as on a wide variety of packaging types. Examples
include plastic ring bottle caps that are detached (broken) when opened
for the first time; stickers on the screw of electronic devices to
prevent them from being opened and labelled with the warning ‘Warranty
will be void if seal is damaged’, or similarly, as tape labelled ‘Do not
accept if seal broken’ on delivered parcels. When seals are removed
(broken), they may enter a new life cycle and become a (cheap)
convenient tool of accounting. For example, to count how many bottles of
water have been consumed in a restaurant during a week, one possibility
would be to collect and count all the caps. In comparison with
collecting the bottle, caps offer the advantages of being smaller, often
standardized, and easy to store, allowing the bottles to be discarded or
reused in the meantime.

These intertwined functions of sealing — integrity, ownership,
obliteration — have been seminal in their adoption by growing
institutions. The necessity of safeguarding integrity and ownership
starts to be critical where administrative activities take place beyond
the face-to-face sphere of interactions. Sealings are crucial for
operations on a larger scale, because they enable control of several
administrative procedures or transactions without the physical presence
of a person or a group of people. As such, it is not surprising that
seals and sealing are an important and integral part of the
Anatolian–Old Assyrian trading and exchange networks during the first
half of the second millennium BC. Merchants organized as
‘family-businesses’ deployed large-scale commercial activities within a
trade network covering the central Anatolian region and the link between
Anatolia and Aššur. The impression of seals on fresh clay to produce a
sealing was a widely used identification technique during this period.
It was applied on different clay supports, on written objects such as
tablets and envelopes, or on clay lumps securing the opening of
objects.[^sealing-terminology] Out of this practical activity emerged a
remarkable craft, seal engraving, and the study of the iconography has
proved to be a very rich field for investigating cultural interactions,
economy, belief systems, and the organization of craft making
[@OzgucN1965a; @OzgucN1968a; @OzgucN1989a; @OzgucN2001a; @OzgucN2006a;
@Alexander1979a; [**p.182 starts**] @Teissier1994a; @Ozkan2010a; @Lassen2014; @Larsen2014;
@Topcuoglu2014;@Ricetti2017; @Ricetti2018].


[^sealing-terminology]: In this paper, *seal* refers to the object used
to seal (cylinder or stamp seal). The main purpose of a seal is to
generate the same imprint at each application. *(Clay) Sealing* refers
to the clay lump that have been sealed, generally with a seal, but
inscription or finger imprints are other possibilities. A sealing is
unique and has a shape dependent on the support. The function of the
support determines the function of the sealing, such as door-sealing,
jar-sealing, etc. See @Ferioli1979; @Ferioli2007; and @Zettler1987 with
similar terminology; at Arslantepe [@Frangipane2007b] *clay sealings*
are called *cretulae*.

For a long time, sealing studies underestimated the importance of the
functions and practices of sealing to disentangle otherwise non-recorded
aspects of transactions. Shifting the interest from art objects to
common practices within a comprehensive context, that is the
relationships of seals and sealings with other administrative tools,
makes it possible to provide another glimpse of the economic system
[@Panagiotopoulos2010a]. The functional study of sealings involves the
study of the impressions of seals onto them as well as the impression of
objects on which the sealing has been applied, including string and
other materials, the form and the material of the sealing, and all the
details that can be collected on the life of the sealing, such as its
creation, usage, and discarding [@Ferioli2007]. Most of the
time the study of sealing is based on sealing debris, i.e. sealings that
have been discarded. Therefore, it is difficult to understand a unique
piece out of context and it is the comparison within and between
‘collections’ put into archaeological context that makes it possible to
reliably interpret how they were used, and consequently deduce what was
(the shape of) the kind of objects sealed.

As a first step toward a functional analysis of sealing practices in
Anatolia during the Anatolian–Old Assyrian Trade Network Period, this
paper aims at providing an overview of collections of sealings at
Kültepe.


# Kültepe

Kültepe provides one of the most important collection of seals and
sealings from the Anatolian–Old Assyrian Trade Network
Period.[^kultepe-overview] There is a strong contrast between the number
and the spatial distribution of texts and sealings discovered at the
site. Around 23,000 cuneiform tablets have been found during the
excavations at Kültepe, most of them in the lower town and only forty
(i.e.  less than 0.2 per cent) on the citadel mound [@Michel2011b]. In
contrast, as of 2005, only 430 clay sealings have been discovered, but
over a quarter (c. 27 per cent, 118 sealings) were found on the citadel
mound [@Michel2016a]. The proportion of clay tablets and clay sealings is
also different from other sites, in which more clay sealings than texts
have been discovered, for instance at Acemhöyük, where more than 1300
sealings but almost no tablets have been found [@OzgucN1989a;
@Veenhof1993; @Veenhof2017a; @Kuzuolu2015a_Acemhoyuk_yazi].[^palace_destruction_texts]

[^kultepe-overview]: There a numerous, easily accessible accounts on
Kültepe such as @OzgucT2003a; @Kulakoglu2011a.

[^palace_destruction_texts]: Nimet Özgüç -@OzgucN2001a proposed that the
relatively low number of sealings at Kültepe results from the
‘destruction’ of the archaeological contexts that the palace underwent
during the first excavations in the nineteenth century AD. It is also
possible that due to the small size of some sealings, they may have been
missed with the archaeological methods of the twentieth century. Under
any circumstances, it is striking that the finds from Kültepe do not
reflect what we know from other settlements of central Anatolia and it
is rather atypical.

A significant part of the c. 23,000 written documents have seal
impressions (especially envelopes). Among them, we distinguish more than
two thousand different seals [@Ozkan2010a]. Two types of seals are
commonly used: cylinder seals, typical from the Mesopotamian traditions,
which have been grouped according to different styles (‘Old Assyrian’,
‘Old Babylonian’, ‘Old Syrian’, ‘Old Anatolian’) and stamp seals,
considered as a marker of Anatolian traditions and purported to be more
frequent during the later period of the Anatolian–Old Assyrian Trade
Period, ‘Karum Level Ib’ [@OzgucN1965a; @OzgucN1989a]. Seals were made
of semi-precious or hard stones, metal, ivory, bones, or faïence
[@OzgucT2003a, 275]. By order of support frequency, seals have been
mainly used on envelopes, clay sealings, and ceramics [@Ozkan2010a].
Clay sealings [often referred to as ‘bullae’ at Kültepe,  see
@Andersson2017] were shaped to fit the closed item or as an independent
‘tag’ and are found in a variety of forms such as triangular,
matchbox-shaped, mushroom-shaped, round (pottery opening stopper), or
hemispheric  [@OzgucT2003a, 288−291]. Seals on envelopes and clay
sealings may be accompanied with written cuneiform text specifically
referring to the context of the sealing. In this case, items were first
sealed then labelled [@OzgucT2003a, 275].

Most of the research on seals has been devoted to the iconography and
prosopography to eventually identify the owner of the seals
[@OzgucN1965a; @OzgucN1989a; @OzgucN2006a; @Teissier1994a; @Lassen2014;
@Ricetti2019]. Recently however, more attention has been directed
towards the production of seals, clay sealings, and sealing practices
[@OzgucN2001a; @OzgucN2006a; @Larsen2008a; @Michel2016a; @Ricetti2017].
This trend has been initiated mostly by philologists looking at the link
between texts and clay sealings found together at the same find-spot to
understand archival practices. It is very fortunate that seals were not
only mentioned in texts, but have also been found together with
texts.^[This is not unique as demonstrated by some of the finds from the
Hittite capital. See @Herbordt2005.]

  [**p. 183 starts**]

## Evidence of Sealing Practices at Kültepe

The written documents found at Kültepe testify to an intensive use of
sealings to manage the commercial activities between Aššur and Anatolia
as well as within Anatolia. The seals belonging to the owners or the
senders of goods (such as metal, textiles, precious objects) were used
to secure containers (made out of textiles, leather bags, baskets, or
reed mats) during their transportation in the merchants’ caravans
[@Stratford2014a]. At the final or interim stocking destination,
merchants sealed the houses, rooms, or containers to protect the
merchandise awaiting further transaction, a practice that was extended
when a merchant died before its succession had been managed
[@Larsen1977a; @Larsen2008a; @Veenhof1993; @Michel2016a;
@Andersson2017].


From the archaeological accounts of the twentieth century ad, we know
that texts and clay sealings have been found together in houses in the
lower town. This situation offers a very good case to investigate the
‘archival practices’ by analysing the archaeological record and the
intertwined relation between texts and seals. However, even for these
best recorded findings, the archaeological accounts do not allow us to
reconstruct specific find-spots and the relations between artefacts and
clay sealings. Until now, in the best cases, it has been possible to
work out which sealings were found with which archive, but this does not
allow us to reconstruct detailed sealing and archiving practices
[@Larsen2008a].  For example, in the case of the Elamma archive
published by Veenhof  [-@Veenhof2017Elamma], the broad description
provided in the archaeological report makes it impossible to reconstruct
the group of tablets and sealings, even if it is clear that tablets were
grouped by dossier or by gender.[^veenhof-gender] In the absence of
detailed archaeological data, the study of Kültepe’s clay sealings
necessarily focuses on the objects themselves. The inscription, if
present, attracts most of the attention, but the general shape and the
traces or marks left on the back of these pieces of clay are
increasingly highlighted. These reflect somehow their use: a flat
surface suggests, for example, the application to a wooden door or box;
but traces of string, reeds, or textiles hint at the closing of a bundle
or cloth bag.[^ozguc-classification] Based on inscriptions on some of
the clay sealings, they have been classified into two categories: clay
sealings for the shipment of goods, for which the inscription in
Assyrian states this with the word _našpertum_, and clay sealings used
for archival purposes [@Veenhof1993; also @Veenhof2013a, 55−58]; Michel
[-@Michel2016a, 177] mentions that these functions are not necessarily
exclusive. The word _našpertum_ indicates that these sealings have been
attached to tablet lots to be sent [for a discussion of the Assyrian
word _našpertum_ see @Tunca2001a, 305−306; @Veenhof2017Elamma, 114;
@Michel2018].

[^veenhof-gender]: ‘The archive of the merchant [Elamma] was found along
the base of the east wall of Room 3 and in rooms 4–5, in groups once
packed in boxes, bags, sacks and straw mats. On top of each group lay
one or two clay sealings. Unopened envelopes were placed at the bottom,
tablets on top. In contrast to other archives, here we did not find
tablets stored in jars’ [@OzgucT1994a, 369]


[^ozguc-classification]: The main publication of the clay sealings
from Kültepe deals with the sealings found until 1997. It classifies the
clay sealings into nine groups [@OzgucN2001a, 135−136]: 1. hemispheric
(one hundred clay sealings) with traces of textile and strings on the
back; 2. triangular clay sealings (twenty-one clay sealings): they have
string holes and inscription, as a kind of tag; 3. gable with cylinder
seal rolled on two sides of wet clay lumps (twenty-two occurrences) with
string marks on back. Closing of small vessels?; 4. cone-shaped, with
string hole on top and impression on base. Eight occurrences, six of
which are from Late Mound palace; 5. rectangular in a shape similar to
tablets (seven cases); 6. half ring, probably attached to the neck of
vessels (seven occurrences); 7. plaques or disc-shaped (five
occurrences); 8. stoppers of jars (mushroom and alike); 9. hollow clay
sealings (two cases). No specific drawing supports this typology. Michel
noted that one of the sealings found in the house of Šumī-abiya, Kt
90/&nbsp;111, has been interpreted as a door sealing, but the sealing
bears an inscription that mentions _našpertum_, a hint that argues against
a door seal. Instead, it could be a wooden-chest containing goods or
tablets, sent from Aššur or or another locality [@Michel2016a, 178].

Which are the seal collections and assemblages we know from
archaeological evidence at Kültepe? We are aware of multiple cases for
which texts and clay sealings have been found together. Nimet Özgüç men-
tions diverse archives of merchants associating texts and sealings, such
as archives of Uzua, found in 1948 [-@OzgucN2001a, 148−150: six
sealings]; Adad-ṣululi also found in 1948 [-@OzgucN2001a, 150−153:
eleven sealings]; or Ali-ahum found in 1950 [-@OzgucN2001a, 157−164:
twenty-four or twenty-five sealings].[^ali-ahum-more-ref] Additionally, Michel [-@Michel2016a]
discusses in greater details four published archives with clay sealings,
the archives of the families of Šumī-abiya, of Elamma, of Šalim-Aššur
and of Ali-ahum[^michels-article] Moreover, besides the clay sealings
found in the lower town, assemblages of seal- ings have been found on
the citadel mound: in the so- called ‘Palace of the west terrace’, in a
house, and in the ‘Waršama palace’ [@OzgucN2001a]. The description and
comparison of the assemblages from the lower town **[page 184 starts]**
and the citadel mound give good hints at how people engaged with clay
sealings in their daily life at Kültepe.


[^michels-article]:For this article, I only will present these four archives as they have recently been published and contextualised into a larger frame.

[^ali-ahum-more-ref]: For further references, see @Michel2000; @Veenhof2019

Even if the publication of the sealings from Kültepe is rich in
illustrations, not all the sealings mentioned in the catalogue include a
photo. Moreover, almost all the photos are close-up views of seal
impressions rather than general pictures showing the shape of the
sealings. This may be the best choice for investigating the
icono­graphy, but it is poor for a typological classification.
Therefore, only a coarse reinterpretation of the typology is possible.
In order to help myself and help the readers to understand what the
sealings look like, I sketched drawings of some representative sealings
from the published photos.  These are idealized and do not represent
reality. My classification was done using the main publication of Nimet
Özgüç [-@OzgucN2001a]. This is not an optimal methodology, and the
classification should be regarded with caution. Due to the fact that I
only used photos and drawings, it wasn’t possible to determine kinds of
marks such as ropes, thread, or strings as rightly already suggested for
Kültepe [@Andersson2017].^[I would like to renew my thanks to Cécile
Michel and Fikri Kulakoğlu for providing me with unpublished photos and
for letting me reproduce some of them here.]


![](img/kt90-k111.png)

> *Caption Figure 1*: Schematic drawing of the hemispheric clay sealing
> Kt&nbsp;90/k&nbsp;111, with placement of seal impressions and
> inscription on the outer part of the sealing (left) and indication of
> ‘rope’ on the reverse of the sealing (right).  Redrawn after the
> photographs in @OzgucN2001a, pl.  99. CC BY Néhémie Strupler

![](img/kt90-k207.png)

> *Caption Figure 2*: Schematic drawing of the hemispheric clay
sealing Kt&nbsp;90/k&nbsp;207, with placement of seal impressions and
inscription on the outer part of the sealing (left) and indication of
‘rope’ on the reverse of the sealing (right). The similarity with the
other hemispheric sealings (see Fig. 1) indicates that it was a common
task. Redrawn after the photographs in @OzgucN2001a, pl. 100. CC BY
Néhémie Strupler


![](img/kt90-k206.png)

> *Caption Figure 3*: Schematic drawing of the hemispheric clay
sealing Kt&nbsp;90/k&nbsp;206, with placement of seal impressions and
inscription on the outer part of the sealing (left) and indication of
‘rope’ on the reverse of the sealing (right). The marks of the ‘ropes’
on the reverse of the sealing are slightly different, but the overall
similarity with the other hemispheric sealings (see Figs 1−2) indicates
that using this kind of sealing was a well-known and mastered task and
that it was applied to a similar object: a flat surface (box?) closed
with ‘ropes’. Redrawn after the photographs in @OzgucN2001a, pl. 99. CC
BY Néhémie Strupler


 **[page 185 starts]**

### Sealings from the Lower Town

#### The Archive in the House of Šumī-abiya

The number of sealings found within the archive of this house is not
clear [@Michel2016a].^[Texts published in @Michel1997a and sealings in
@OzgucN2001a: 220--221. See now @Ricetti2019 for a meticulous analysis
of style and seal owner's identification.] Two inscribed clay sealings
are described as "*našpertum*" [see Figs 1−2: Kt&nbsp;90/k&nbsp;111 and
Kt&nbsp;90/k&nbsp;207; @Tunca2001a; @Michel2016a]. The fact that their
forms are very similar consolidates the interpretation of a similar use:
a hemispheric shape with a slightly concave back, and cloth imprints
with string grooves.^[This could be also the case for Kt&nbsp;90/209,
but the inscription is broken.] Kt&nbsp;90/k&nbsp;206 has an almost
identical shape and based on its inscription, this sealing was probably
used for closing a container holding verdicts issued by the _kārum_ of
Kaneš (see Fig. 3).

These three sealings, according to their inscriptions, were used for
sealing lots of tablets. They are very similar in their form, which is
hemispheric, with a flat reverse and string marks. The sealing Kt
90/&nbsp;209 belongs to the same group, but only the personal names
(henceforth PN) are readable in the remaining inscription. It could,
however, also be a *našpertum*.

The two other sealings (­graphically) known from this archive are
different in their form and are anepigraphic. Kt&nbsp;90/&nbsp;210 was
attached to a circular object, which could have been a (door) peg or the
top of a bag (see Fig. 10.4). Kt&nbsp;90/&nbsp;212 is only illustrated by a
closeup view of its seal so it is difficult to draw any information on
its shape, but it seems that the reverse was not flat, making it clearly
different from the three hemispheric seals.

As a possible interpretation of the function of the sealings Kt
90/&nbsp;210 and Kt&nbsp;90/&nbsp;212, I can imagine a system that is
similar to Arslantepe, where some sealings were used for closing a bag
with a peg and a rope (see Fig. 10.5). This could be used for
(long-term) storage of goods (reserve of silver?). I am against
interpreting this as a sealing used for a door peg or for closing
another type of goods that would have been frequently accessed. The low
number of sealings discovered indicates that the sealings we find within
houses were not broken on a regular basis (as would presumably happen in
the case of a door).


![](img/kt90-k210.png)

> *Caption Figure 4*: Kt&nbsp;90/k&nbsp;210, with placement of seal impression
on the outer part of the sealing (left) and indication of ‘rope’ on the
reverse of the sealing (right). The marks of the ‘ropes’ on the reverse
of the sealing are parallele and the general shape of the sealing is
round, indicating that it was applied on a round surface such as a peg
on which a rope was winded (see Fig. 5).  Redrawn after the
photographs in @OzgucN2001a, pl. 100. CC BY Néhémie Strupler


![](img/Ferioli2007-Fig_II-7.png)

> *Caption Figure 5*: Possible interpretation for rounded sealings
with parallel rope marks. One the left, clay was applied on ropes wound
around a peg. This would explain the round shape and the parallel rope
for some sealings from Kültepe. The detail on the right of the figure
presents the situation prior applying the fresh clay to seal. Inspired
by @Ferioli2007 [p. 75, fig II.7]. CC BY Néhémie Strupler


To recapitulate, we can classify the published sealings into two
groups. First the group of hemispheric sealings associated with sent
lots (only of tablets?), with or without a _našpertum_ inscription. Second
the anepigraph sealings with rounded shapes. In general, the low
number of sealings and the absence of reusing the same seal on different
sealings indicates a long-term archival practice rather than a daily
practice, in which the sealing would have been regularly broken and
renewed. The similarities in the hemispheric sealing type tend to
indicate that, despite the low number of executed sealings, it was a
rather well-known and shared practice among merchants. As we will see,
the corpora from other merchants’ houses from the lower town display a
similar pattern.

**[page 186 starts]**

#### Sealings in the House of Elamma

This house was excavated in 1991 and 1992 [latest account in
@Veenhof2017Elamma, 13−18], and seven clay sealings were found with it
[@OzgucN2001a: 225−226 for the sealings, 340 for the reading;
@Veenhof2017Elamma, 474−476 discusses the inscriptions from the sealings
within the text-archive context]. Two sealings (Kt&nbsp;91/k 378 and
Kt&nbsp;91/405) are very similar to clay sealings within the
text-archive context). Two sealings (Kt&nbsp;91/&nbsp;k 378 and Kt
91/&nbsp;405) are very similar to clay sealings from the Šumi-abīya
house: hemispheric with a slightly concave back, cloth imprints, deep
perpendicular string grooves. The sealing Kt&nbsp;91/&nbsp;405 bears the
word _našpertum_, and therefore was most probably used for sealing
tablets.  Two other sealings are also hemispheric and bear inscriptions
that only mention the owner of the seal (‘PN1 son of PN2’ for
Kt&nbsp;91/&nbsp;k 380 and Kt&nbsp;92/&nbsp;k 141), that would also
include them in the _našpertum_ type [for this interpretation see
@Tunca2001a, 305−306]. The other sealings (Kt&nbsp;91/&nbsp;k 379,
Kt&nbsp;91/&nbsp;k 553, Kt&nbsp;92/&nbsp;k 140) are described as
‘shapeless’ by Nimet Özgüç [-@OzgucN2001a, 225−226]. Kt&nbsp;92/&nbsp;k 140
and Kt&nbsp;91/&nbsp;k 379 have a concave back, which suggests a
difference with the hemispheric sealings. They were probably applied on
a softer material (sack?). Moreover, Kt&nbsp;91/&nbsp;k 379 is
remarkable because it has fourteen impressions of the same stamp seal.
Sadly, we do not have any information about where this sealing was
found, as it is the only example that is so different. If we agree with
the general idea that most of the sealings have been used for archiving
and managing the texts, stamp seals seem (to me) to be less adequate for
this aim. This would point to the interpretation that this sealing was
closing some other good. I could not find any information about what the
break tells us: Was the sealing kept broken, as a reminder that some
transaction happened, or was it still attached to something (a sack?) at
the moment of abandonment and destruction of the house?


![](img/kt91-k379.png)

> *Caption Figure 6*: Schematic drawing of the clay sealing
Kt&nbsp;91/k&nbsp;379, with the repetition of the same seal (originally
St&nbsp;72). The general shape, round with traces of string and fabric
on the inside points to a sealing that was applied on the closing of a
sack, but without a peg. Redrawn after the photographs in @OzgucN2001a,
pl. 108. CC BY Néhémie Strupler


![](img/kt93-k810-RF.jpg)

> *Caption Figure 7*: Kt&nbsp;93/k&nbsp;810: photos of the obverse and
reverse. This shows a typical hemispheric sealing that was applied on a
relatively flat surface tied with strings. The finger gives a good
impression of the size of the sealing. See also @OzgucN2001a, pl. 123.
Photo: Cécile Michel, ©Kültepe archaeological mission


![](img/kt93-k258-RF.jpg)

> *Caption Figure 8*: Kt&nbsp;93/k&nbsp;258: photo of the obverse and
reverse. The photo on the right shows how the seal is bent in its
middle, where the main rope was covered. This sealing belongs however to
the group of the hemispheric type. One of the few cases of sealing
removed shortly after having been applied (see Fig 9). Publication by
@OzgucN2001a, pl.  115. Photo: Cécile Michel, ©Kültepe archaeological
mission



#### Sealings from the House of Ali-ahum

Two adjacent houses containing text archives that have been excavated in
1993 contained 926 tablets and 37 clay sealings constituting the
archives of Ali-ahum, son of Iddin-Suen, and his son Aššur-taklāku
[@Michel2008b; @Michel2016a]. Among these sealings, the majority (22
sealings, 60 per cent) are of the hemispheric type:
Kt&nbsp;93/k&nbsp;254, Kt&nbsp;93/k&nbsp;255, Kt&nbsp;93/k 256,
Kt&nbsp;93/k&nbsp;257, Kt&nbsp;93/k&nbsp;258 (see Fig. 8),
Kt&nbsp;93/k 259 (see Fig. 9)^[Judging from the photographs kindly
provided by Cécile Michel, these two sealings were also hemispheric
(Kt&nbsp;93/k&nbsp;258, Kt&nbsp;93/k&nbsp;259), but in my opinion they
were deformed at the moment they were broken when the clay was
still fresh. This would also explain why the impressions of the seals
are also notably badly conserved.], Kt&nbsp;93/k&nbsp;260,
Kt&nbsp;93/k&nbsp;261, Kt 93/k&nbsp;263 (inscribed as *našpertum*),
Kt&nbsp;93/k&nbsp;264 (inscribed as *našpertum*), Kt&nbsp;93/k&nbsp;268
(inscribed as *našpertum*), Kt&nbsp;93/k&nbsp;271 (inscribed as
*našpertum*), Kt&nbsp;93/k&nbsp;272 (inscribed as *našpertum*)**[page
187 starts]**, Kt&nbsp;93/k&nbsp;273 (inscription of the clay sealing
indicates that it was used for closing tablets concerning Suen-pilah),
Kt 93/k&nbsp;608, Kt&nbsp;93/k&nbsp;801 (the back is impressed with
concentric reeds), Kt&nbsp;93/k&nbsp;802 (shape is slightly different,
but this looks like a sealing that has been removed when the clay was
still wet, explaining the deformation), Kt&nbsp;93/k&nbsp;804 (inscribed
as *našpertum*), Kt&nbsp;93/k&nbsp;807 (with an inscription indicating
that it was closing a group of tablets),  Kt&nbsp;93/k&nbsp;808
(inscribed as *našpertum*), Kt&nbsp;93/k&nbsp;810 (see Fig. 7), and
Kt&nbsp;93/k 811.

![](img/kt93-k259-FR.jpg)


> *Caption Figure 9*: Kt&nbsp;93/k&nbsp;259, photo of the obverse and
reverse. In comparison to the hemispheric sealing type, it shows that
the clay was still fresh when it was removed from the string it was
covering. Publication by @OzgucN2001a, pl.  115. Photo: Cécile Michel,
©Kültepe archaeological mission

![](img/kt93-k809-RF.jpg)

> *Caption Figure 10*: Kt&nbsp;93/k&nbsp;809: photo of the obverse and
reverse, similar to Kt&nbsp;90/k&nbsp;210, interpreted as a sealing of a
sack with a peg (see Fig. 5).  Photo: Cécile Michel, ©Kültepe
archaeological mission, publication by @OzgucN2001a, pl.  123


![](img/kt93-k812-RF.jpg)

> *Caption Figure 11*: Kt&nbsp;93/k&nbsp;812: photo of
the obverse and reverse, similar to Kt&nbsp;90/k&nbsp;210, interpreted
as a sealing of a sack closed by a peg (see Fig. 5). Photo: Cécile
Michel, ©Kültepe archaeological mission, publication by @OzgucN2001a,
pl. 124


![](img/kt93-k267-RF.jpg)

> *Caption Figure 12*: Kt&nbsp;93/k&nbsp;267: photo of the mushroom-type
sealing with cylinder and stamp seals impressions. On the reverse
(right), the rim as well as three ‘rope’ imprints are clearly visible.
The shape corresponds to similar findings and reconstruction from
Arslantepe (see Fig 13). Photo: Cécile Michel, ©Kültepe archaeological
mission, publication by @OzgucN2001a, pl.  117

There are ten sealings with a different shape. Among them, four have a
longer (broken) inscription (Kt&nbsp;93/k 269, about an offering;
Kt&nbsp;93/k&nbsp;270, about a sales transaction, Kt&nbsp;93/k&nbsp;805,
and Kt&nbsp;93/k&nbsp;806) classifying them as sealings for text
archival purposes. The other four sealings (Kt&nbsp;93/k&nbsp;262,
Kt&nbsp;93/k&nbsp;809, Kt&nbsp;93/k&nbsp;812, Kt 93/k&nbsp;814), judging
from the photos, seem to have been applied to a rounded surface, like a
peg winded with ropes closing a bag (see Fig 10−11).
Kt&nbsp;93/k&nbsp;267 is a mushroom-shaped sealing (see Fig. 12) with
cylinder and stamp seal impressions. It is larger than the other
sealings and it is still possible to recognise the rim of the vessel on
the reverse of the sealing. It seems that the clay was not directly
applied on the vessel but over a tissue that have been tied with ropes,
in a similar way as it has been proposed for Arslantepe (see Fig. 13).

The remaining five sealings are too small to be classified into types
(Kt&nbsp;93/k&nbsp;265, Kt&nbsp;93/k&nbsp;266, Kt 93/k&nbsp;650
(deformed hemispheric seal?) Kt&nbsp;93/k&nbsp;803, Kt 93/k&nbsp;815).


<!--

 - Kt 93/k 254 = hemispheroid, same unknown seal
 - Kt 93/k 255 = hemispheroid, same unknown seal
 - Kt 93/k 256 = hemispheroid, same unknown seal
 - Kt 93/k 257 = hemispheroid, same unknown seal
 - Kt 93/k 258 > not depicted
 - Kt 93/k 259 = around string
 - Kt 93/k 260 = seal + hemisphroide like 254−257
 - Kt 93/k 261 = seal + hemisphroide like 254−257
 - Kt 93/k 262 = around string
 - Kt 93/k 263 > shipment (našpertum)
 - Kt 93/k 264 > shipment (našpertum)
 - Kt 93/k 265 > ?
 - Kt 93/k 266 = frgmt
 - Kt 93/k 267 = mushroom + seal + stamp + string
 - Kt 93/k 268 > shipment (našpertum)
 - Kt 93/k 269 > offering
 - Kt 93/k 270 > ? (long text about selling)
 - Kt 93/k 271 > shipment (našpertum)
 - Kt 93/k 272 > shipment (našpertum)
 - Kt 93/k 273 > archive
 - Kt 93/k 608 = same unknown seal
 - Kt 93/k 650 = perpendicular strings

 - Kt 93/k 801 = concentric reed (basket)
 - Kt 93/k 802 > ?
 - Kt 93/k 803 = frgmt
 - Kt 93/k 804 > shipment (našpertum)
 - Kt 93/k 805 > ? (long text)
 - Kt 93/k 806 > archive
 - Kt 93/k 807 > archive
 - Kt 93/k 808 > shipment (našpertum)
 - Kt 93/k 809 = seal of Aššur-taklāku (House owner) + basket
 - Kt 93/k 810 = seal + read + string
 - Kt 93/k 811 > ?
 - Kt 93/k 812 = frgmt
 - Kt 93/k 813 > archive
 - Kt 93/k 814 = frgmt
 - Kt 93/k 815 = frgmt
 -
 - text: Michel 2008, 2015
 - Seal: N. Özgüç et Tunca 2001 : 229−235, 342−345.

-->

![](img/Ferioli2007-Fig_II-13.png)

> *Caption Figure 13*: Possible use for mushroom-type sealings used for
closing a vessel on top of a piece of fabric fixed with string.
Inspired by @Ferioli2007, 82, fig II.13. CC BY Néhémie Strupler

**[page 188 starts]**

#### Sealings from the House of Šalim-Aššur

According to Morgen Larsen, who published this archive discovered in
1994, 1100 texts and thirty-three clay sealings were found in the house of
Šalim-Aššur and his two sons Ennam-Aššur and Ali-ahum.^[See
@Larsen2008a for the sealing; texts are published in @Larsen2010a;
@Larsen2013a; @Larsen2014a; @Larsen2018a] The typology of the sealings
matches well with the other archives, but displays also very interesting
differences as well.

A new type is present, the triangular shape, for which most of sealings
are inscribed (seven out of ten) : Kt 94/k 876, Kt&nbsp;94/k&nbsp;878
(inscription about a group of tablets), Kt&nbsp;94/k&nbsp;879
(inscription about a group of tablets), Kt&nbsp;94/k&nbsp;1058,
Kt&nbsp;94/k&nbsp;1060, Kt&nbsp;94/k&nbsp;1061 (inscription about a
group of tablets), Kt&nbsp;94/k&nbsp;1062 (inscription about a group of
tablets), Kt&nbsp;94/k&nbsp;1063 (inscription of PN from the seal
owner), Kt&nbsp;94/k&nbsp;1290 (inscription about a group of tablets),
Kt&nbsp;94/k&nbsp;1664 (inscription about a group of tablets). At least
twelve sealings belong to the hemispheric group, out of which eight bear
an inscription, either with the word *našpertum* or indicating a group
of tablets or the recipient: Kt&nbsp;94/k&nbsp;586 (inscription about a
group of tablets), Kt&nbsp;94/k&nbsp;646, Kt&nbsp;94/k&nbsp;648
(*našpertum*), Kt 94/k&nbsp;875, Kt&nbsp;94/k&nbsp;877 (indication of
the recipient), Kt 94/k&nbsp;1059 (indication of the recipient),
Kt&nbsp;94/k&nbsp;1184 (*našpertum*), Kt&nbsp;94/k&nbsp;1185
(*našpertum*), Kt&nbsp;94/k&nbsp;1286 (inscription about a group of
tablets), Kt&nbsp;94/k&nbsp;1288, Kt&nbsp;94/k&nbsp;1289,
Kt&nbsp;94/k&nbsp;1431 (inscription about a group of tablets). Four
inscribed sealings that are badly broken could belong to one of these
two groups : Kt 94/k&nbsp;874 (statement of name sealing),
Kt&nbsp;94/k&nbsp;1665 (inscription about a group of tablets),
Kt&nbsp;94/k&nbsp;1684 (only part of the PN preserved),
Kt&nbsp;94/k&nbsp;1717 shipment (inscription of the PN sealing). Out of
the last seven sealings, three are "odd shaped" (Kt&nbsp;94/k&nbsp;647,
Kt&nbsp;94/k 1287, Kt&nbsp;94/k\ 1531) and the remaining four sealings
have been sealed with stamp seals; two sealings, Kt 94/k&nbsp;585 and
Kt&nbsp;94/k&nbsp;1729 are in the form of a jar stopper (similar in
appearance to Kt&nbsp;91/k&nbsp;371 from the Elamma Archive), two
sealings with stamp seals, Kt&nbsp;94/k 1430 and
Kt&nbsp;94/k&nbsp;1489, are not classifiable with the photos provided.

From this classification, it is easy to conclude that almost all of the
sealings were used for archival purposes (twenty-two including the
triangle and hemispheric types, summing to twenty-six if the smaller
fragments are also taken into account). If the function of the three
‘oddly shaped’ seals is unclear, only four seals (i.e. 12 per cent), all
with stamp seals, may have been used for a purpose other than text
archival practices.

<!--

Šalim-Aššur:

 - sealing @OzgucN2001a:
   a) p. 240 (from Kt 94/k 874) to p. 247 (to Kt 94/k 1729) = 28 clay sealings
   b) p. 239 (from Kt94/k 585) to p. 240 (to Kt 94/k 648) = 5 clay sealings
 - texts and sealing : @Larsen2008a

Among the sealings, 19
clay sealings have an inscription.





From the text he deduced that 8 clay
sealings were used to identify shipments of goods or
tablets  and 11 others would have had a archival
function, allowing identification of batches of tablets
(Larsen 2008). He recognise that most of the tablets
with archival function are triangular with 3 string
holes [Larsen2008a,87], one of the them 1431 with marks
of basket
 - Kt 94/k 585 > pitch stopper with stamp seal
   impression. Looks like Kt 91/k 371 from the Elamma
House
 - Kt 94/k 586 > hemispheroid, inscription archive
 - Kt 94/k 646 > hemispheroid, ?
 - Kt 94/k 647 > shapeless (not depicted)
 - Kt 94/k 648 > hemispheroid,shipment
 - Kt 94/k 874 > Frgmt, shipment (statement of name sealing)
 - Kt 94/k 875 > hemispheroid
 - Kt 94/k 876 > Gabled
 - Kt 94/k 877 > hemispheroid, shipment
 - Kt 94/k 878 > triangular, archive
 - Kt 94/k 879 > trianbular, archive
 - Kt 94/k 1058 > Gabled
 - Kt 94/k 1059 > hemispheroid, shipment
 - Kt 94/k 1060 > Gabled
 - Kt 94/k 1061 > triangular, archive
 - Kt 94/k 1062 > triangular, archive
 - Kt 94/k 1063 > Gabled, shipment (statement of name sealing)
 - Kt 94/k 1184 > hemispheroid, shipment
 - Kt 94/k 1185 > hemispheroid, shipment
 - Kt 94/k 1286 > hemispheroid, archive
 - Kt 94/k 1287 > odd shaped
 - Kt 94/k 1288 > hemispheroid
 - Kt 94/k 1289 > hemispheroid
 - Kt 94/k 1290 > triangular, archive
 - Kt 94/k 1430 > odd shaped
 - Kt 94/k 1431 > hemispheroid, archive (marks of basket)
 - Kt 94/k 1489 > odd shaped
 - Kt 94/k 1531 > pill shaped
 - Kt 94/k 1664 > triangular, archive
 - Kt 94/k 1665 > archive
 - Kt 94/k 1684 > archive (?badly broken)
 - Kt 94/k 1717 > shipment (statement of name sealing)
 - Kt 94/k 1729 > pitcher stopper

-->

#### Remarks on the Finds from the Lower Town

The evidence presented here from the lower town of Kültepe points toward
the use of sealings as an internal management tool within the house. The
majority of the sealings have been used for organizing and storing
tablets. Within this group, an important part of the sealings have been
used to indicate the origin or the destination of a group of tablets
(_našpertum_). Only very few examples suggest the storage of other
goods. Several clues hint at the interpretation of all of these sealings
as tools for long-term storage and archival purposes rather than
commercial transactions or the transportation of goods: the low number
of sealings, their types, the near-absence of different sealings
attesting to the use of the same seal, and the lack of any indication
showing that the same actions have been repeated in a short time span.
Finds from the mound, however, display greater diversity.

### Sealings from the mound

Similar to the situation in the lower city, the exact number of sealings
from the mound is difficult to assert. The mentions in the
archaeological reports do not fully correspond with the published
material and with the catalogues from the two main publications by Nimet
Özgüç [-@OzgucN1989a; -@OzgucN2001a].^[There are some discrepancies
between the publications and the authors. For example, the sealing
Kt&nbsp;z/t&nbsp;28 is mentioned as part of the sealings from Room 12 of the
Waršama Palace by Tahsin Özgüç [@OzgucT1999a, 87], but is absent in the
publication of Nimet Özgüç [-@OzgucN2001a].] However, the differences
with the sealings from the lower city are striking [@Larsen2008a]. Most
of the sealings from the mound are anepigraphic and have been sealed
with stamp seals, and not with cylinder seals. Sealings from the mound
in context include those from a house, the so-called palaces ‘on the
west terrace’ and ‘Waršama’ [@Michel2019].


#### The ‘Palace on the West Terrace’

This building  is dated to the ‘Karum level II’, contemporary to the
‘old palace’ [@OzgucT1999a; summary in @OzgucT2003a, 133--140]. It is an
imposing structure organized around a paved courtyard, with two wings on
each side of an imposing corridor. The number of sealings found in the
building is not unequivocal.  Tahsin Özgüç [-@OzgucT2003a, 139] wrote
that the building has been emptied and that in the debris, only one
sealing ‘with cylinder seal impression’ have been found (corresponding
to Kt k/t 115). However, Nimet Özgüç [-@OzgucN2001a,166−167] mentions
three other sealings from the building (Kt&nbsp;i/t 282−284). I follow
here her catalogue and count four sealings (Kt&nbsp;k/t 115, Kt&nbsp;i/t
282, Kt&nbsp;i/t 283, Kt&nbsp;i/t 284).

**[page 189 starts]**

![](img/kti-t282.jpg)

> *Caption Fig. 14*: Kt&nbsp;i/t&nbsp;282. On the right, the top shows
the multiple application of the same seal, on the left, the reverse
marks indicate the triangle shape left by the beak of a spouted jar. No
visible trace of rope. Photos from the publication of @OzgucN2001a,
pl. 58

![](img/kt77-t85.png)

> *Caption Fig. 15*: Kt&nbsp;77/t&nbsp;85: under the stamp seal, the
trace of a ‘rope’ is visible where the sealing is broken. The style as
well as the sealing type differ from other sealings from Kültepe.
Annotated photo from @OzgucN2001a, pl.  81.


![](img/Bo64-153-2.jpg)

> *Caption Fig. 16*: Bo64-153-2: photo of a sealing
discovered in a house from the later period of the Anatolian−Old
Assyrian trade period. The sealing is similar to Kt&nbsp;77/t&nbsp;85
(see Fig. 15), as well as the size and type of the seal. The
iconography is not identical but shares a common ground. Photo *Archiv
der Boğazköy Expedition*


On the top of Kt&nbsp;i/t&nbsp;282 (see Fig. 14), the same stamp seal
have been impressed ten times, and the reverse of the sealing shows a
triangle imprint, maybe from a beak-spouted vessel [@OzgucN2001a, 166].
Kt&nbsp;i/t&nbsp;284 is also described as a stopper of a beak-spouted vessel
with seven impressions of the same stamp seal on the obverse, but only
the top of the sealing is represented in the publication and no
additional information is provided [@OzgucN2001a, pl. 58]. The sealing
Kt&nbsp;i/t&nbsp;283 is described as being ‘shapeless’.  It has eight
impressions of the same stamp seals.  Again, only a close-up of the
obverse of the sealing is published, giving no further hint on the
general shape [@OzgucN2001a, pl. 58].  The sealing with the cylinder
seal impression (Kt&nbsp;k/t&nbsp;115) is badly broken and is presented
as a ‘band-shaped ring’ with few preserved cuneiform signs indicating
the seal owner.




<!--

- 4 anepigraphic sealing
  - 1957: Kt&nbsp;i/t 282−284: OzgucN1989: 389, pl. 120.2
  - 1959: Kt&nbsp;k/t 115 OzgucT1999a: pl. 100.4 : Beaked pither
-->

#### A House on the Mound

A house with ‘a storeroom’ is known to have been excavated in 1977
[@OzgucN1989a, 382; @OzgucN2001a, 191. I could not identify a report on
the excavations or a plan of the house]. The catalogue lists
seventy-five sealings found in this room. All the sealings are
anepigraphic and have been sealed with stamp seals. Out of the
seventy-five sealings, only twenty are illustrated, and I could not find
any information about the reverse of these seals or their possible
functions. There are, however, multiple cases of sealings that have been
impressed with the same stamp seal showing that some actions have been
repeated and the sealings have been kept for accountancy, similar to
what we know from Arslantepe [@Frangipane2007b].^[Nimet Özgüç thought
that in some case, different fragments belonged to the same sealing.
This is the case with Kt&nbsp;77/t&nbsp;13, mentioned in on catalogue as
‘Black, non-joining fragments of a bulla, very fragmentary design of No.
137’ [@OzgucN1989a, 397]. This sealing is not listed in her 2001
publication anymore [@OzgucN2001a, 191]. I would be cautious about this
interpretation, because without any other indication, we should
hypothesize to have two different sealings with impressions from the
same seal.  This indicate that the number of seventy-five sealings found
in the storeroom should be regarded as a minimum number of sealings.]
The focus was on the iconography and the reconstruction of the different
stamp seals rather than  on understanding the functions **[page 190
starts]** or how they differ from the sealings found in the lower town
[@OzgucN1989a,397−400, pl. 112−115; @OzgucN2001a,191−202, pl. 80−82].
Because they do not bear any inscription and are not associated with
cuneiform tablets, they have not beenrestudied by philologists recently
. As yet, almost no contextual information is available.  Among the
available photos, some may display ‘rope marks’, as Kt&nbsp;77/t&nbsp;85
(see Fig. 15).  However, though the information is scanty, there is no
doubt that these sealings are very similar to an ensemble of *c.* 150
fragments of clay sealings discovered in 1963 and 1964 during the
excavations at Büyükkale, the citadel of the Hittite city Ḫattuša by the
*Boğazköy Archaeological Expedition* [see Fig.  16, and see @Neve1982,
22−30]. These were hidden in the wall of one the best-preserved
buildings from the Anatolian−Old Assyrian trade network period under the
remains of the Hittite royal palace. On the floor of this elaborate
house of more than ten rooms, archaeologists found a rich assemblage of
hundred vessels, including finely modelled rhytons, confirming that this
building certainly belonged to the elites [@Strupler2013d].  At
Boğazköy/Hattuš, about thirty different stamp seals have been
reconstructed out of the 150 clay sealings. They also have been
succinctly published, with particular attention to the iconography
rather than their function.^[A project in collaboration with Andreas
Schachner was initiated in order to republish this material now kept in
the Museum of Anatolian Civilizations in Ankara. We hope that our work
will help to better understand both contexts.] Both assemblages, from
Boğazköy and Kültepe depict animals such as lions, birds or bulls, but
also mythical figures. They are all very similar in their style and
form, with a diameter of 2 cm (see Fig. 15−16).

![](img/ktt-t9.jpg)

> *Caption Fig. 17*: Kt t/t&nbsp;9 (Room 11): this mushroom shaped
sealing is smaller than examples known from the lower city (see Fig.
 12) and shows a distinctive rope mark  on the side, almost
identical to Kt t/t&nbsp;24 (see Fig. 17) Annotated photo from the
publication [@OzgucT2003a, 289, fig. 337; see also @OzgucT1999a, pl.
70.3; @OzgucN1989a, pl.  116.1].

![](img/ktz-t24.jpg)

> *Caption Fig. 18*: Kt&nbsp;t/t&nbsp;24: Photo from another mushroom
type sealing with stamp seals and rope impressions. This type is
almost identical to Kt&nbsp;t/t&nbsp;9 (see Fig. 16).  Photo: Cécile
Michel, ©Kültepe archaeological mission, publication by @OzgucN2001a,
pl. 78



#### Waršama Palace

In the so-called Waršama palace, contemporary to the "Karum level Ib",
ca. thirty-two clay sealings have been discovered.^[As in other cases,
the exact number is not easy to establish. In 1989, N. Özgüç states that
ten sealings came to light [@OzgucN1989a, 382], but her catalogue lists
more than ten sealings from the palace; Tahsin Özgüç says that most of
the sealing have been published in 1989 by Nimet Özgüç, but he mentions
one sealing (Kt&nbsp;z/t&nbsp;28) that is absent from Nimet"s
publications [@OzgucT1999a, 87]); Morgen T. Larsen speaks of
twenty-seven sealings, but does not list them so that the origin of this
number is enigmatic [@Larsen2008a, 83].  There are 32 entries concerning
the Waršama palace in @OzgucN2001a; some numbers are also different from
one publication to the other, such as Kt&nbsp;h/t&nbsp;324 in
@OzgucN1989a, 389, which is equal to Kt&nbsp;h/t&nbsp;326 in
@OzgucN2001a, 183.] They are all anepigraphic and stamp **[page 191
starts]** sealed, except for one, Kt&nbsp;z/t 15, which is a triangular
sealing that mentions a merchant and a votive offering.  All the other
sealings bear stamp seal impressions  ‘typical from the later period’.
Tahsin Ozgüç [-@OzgucT1999a,87] gives the most extensive description of
these sealings: ‘All of the [sealings] have round bases, impressed with
a stamp-seal design; they are reddened and soot-stained as a result of
the conflagration. String-holes are evident. One contingent was tied to
vessels in bottle-shape’. Mushroom-shaped sealings are illustrated by
the sealing Kt&nbsp;t/t&nbsp;9 [see Fig. 16, @OzgucT1999a, pl.
70.3.a/b] and Kt&nbsp;z/t&nbsp;24  [see Fig. 17, @OzgucT1999a, pl.
72.2.a/b]. <!-- Even if they are mushroom shaped, it seems to me that
the interpretation as "bottle stopper" may be inexact.  This
interpretation is not explaining, why  A bottle stopper would be either
applied on the whole opening, without strings between the sealing and
the vase aperture (impeding to be airtight). Admittedly, from a modern
perspective (and my personal expectations), it is surprising, but the
mushroom is not very frequent in general, especially at other sites
(contra Fig. 18). Otherwise--> There are clear string marks on
each side of the ‘mushroom’. Therefore, it is possible to say that
opening of the vase was first closed with a tissue and the sealing was
applied on top of it, as seen in the lower city (see Fig. 12−13).

The iconography of the seals is different from the 1977 house from the
mound, and the iconography of the stamp seals is closer to the Old
Hittite style [@Boehmer1987]. The sealing catalogue provides information
such as in which room of the palace the sealings have been found, but as
it is impossible to classify the sealing into a typology from the
close-up photos and the descriptions, as yet, no functional difference
can be established between the rooms. I am convinced however, that a new
study of these sealings, based on the typology and the archaeological
context, would bring new informations on the function of the sealings
and the rooms as well as some aspects of daily life in the palace.



<!--

##### Room 11

3 clay sealings tied to bootle:

 - Kt t/t 9  = OzgucT1999a, pl. 70.3; OzgucN1989, pl. 116.1
 - Kt t/t 10 = OzgucT1999a, pl. 71.1
 - Kt t/t 11 = OzgucT1999a, fig. A.28

##### Room 12

10 clay sealings:

 - Kt z/t 4   = OzgucN2001, pl. 77
 - Kt z/t 5   = OzgucN2001, pl. 77
 - Kt z/t 6   = OzgucN2001, pl. 77
 - Kt z/t 7   = OzgucN2001
 - Kt z/t 8   = OzgucN2001
 - Kt z/t 9   = OzgucN2001
 - Kt z/t 15  = OzgucN2001, pl. 78
 - Kt z/t 23  = OzgucN2001, pl. 78
 - Kt z/t 24  = OzgucN2001, pl. 78
 - Kt z/t 25  = OzgucN2001, pl. 78
 - Kt z/t 28  = OzgucT1999a, 87 "concentric circles and unclear figures", NOT MENTIONNED IN OZGUCN2001

##### Room 14

half preserved clay sealings:

 - Kt z/t 27  = OzgucT1999a, 88 et fig. A 30, NOT MENTIONNED IN OZGUCN2001

##### Room 25:

 - Kt 73/t 30 = OzgucN2001, pl. 79

##### Room 29:

 - Kt u/t 1  = OzgucT1999a, pl. 73.2;  OzgucN2001, pl. 18

##### Room 44:

 - Kt s/t 64 = OzgucN2001, pl. 31

##### Room 46: 7 clay sealings:

 - Kt v/t 22  = OzgucT1999a, pl. 71.2; OzgucN1989, pl. 117.7 (same as Kt v/t 43)); OzgucN2001, pl. 76
 - Kt v/t 41  = OzgucT1999a, pl. 71.2; (same as Kt v/t 42, 44); OzgucN2001, pl. 76
 - Kt v/t 43  = OzgucT1999a, pl. 71.2; OzgucN1989, pl. 117.7
 - Kt v/t 44  = OzgucT1999a, pl. 71.3; OzgucN1989, pl. 117.9; OzgucN2001, pl. 76
 - Kt v/t 45  = OzgucT1999a, pl. 71.4; OzgucN1989, pl. 117.8
 - Kt v/t 46  = OzgucT1999a, pl. 71.5; OzgucN1989, pl. 117.8

##### Room 51:

 - Kt 74/t 13 = OzgucN2001, pl. 79

##### Debris:

 - Kt g/t 280  = OzgucT1999a, pl. 74.2; OzgucN2001, pl. 57
 - Kt h/t 102  = OzgucN2001, pl. 57
 - Kt h/t 318  = OzgucN2001, pl. 57
 - Kt h/t 324  = OzgucN1989, pl. 114−115
 - Kt h/t 326  = OzgucN2001, pl. 57
 - Kt h/t 327  = OzgucN2001, pl. 57
 - Kt o/t 204a  = OzgucN2001, pl. 59
 - Kt y/t 12  = OzgucN1989
 - Kt z/t 4  = OzgucT1999a, pl. 74.3; OzgucN2001, pl. 57

27 clay sealing all except one without writing. The one inscribed (Kt z/t 15)
is a triangular one and the text runs "Sealed by Ili-dan and Salim-Assur; for
the merchants; concerning the votive offering". All the other bulae carry
stamp seals typical of the later period



#### Origin unclear

 - 1954: Kt F/t 397, im building in R/39: OzgucN1989: 389,56;  pl. 118−119

 - 1975: Kt 75/t 1: no indication of origin.

-->

![](img/alisar_c2705.png)

> *Caption Fig. 19*: Sealing c 2705 from Alişar Höyük,
described as "Fragment of a pottery stopper with string marks and part
of a stamp seal impression with circular base; design unrecognizable.
Found 5.20 deep in T 29", @Osten1937b, p227 and figure 253, p.  216



![](img/Alp1968-T130.jpg)

> *Caption Fig. 20*: A sealing from (Konya) Karahöyük [@Alp1968a, catalogue
130], with almost identical marks as Kt&nbsp;93/k&nbsp;809 and Kt
93/k&nbsp;212 (see Fig 10−11)

#### Remarks on the Finds from the Mound

The discoveries from the citadel mound are different from the lower
city, with an almost exclusive usage of the stamp seals. Sadly, the
published information is not sufficient to create a new typology in
order to understand what the sealings were closing, but because they
have not been found together with tablets, we can exclude their usage as
‘text archival’ tools. They must have sealed containers, but the main
question remains open: Was the sealing used for internal management of
storage within a building or for keeping track of exchange outside of
the building? Only the findings from the house, showing that the same
seal has been repeatedly used on different sealings, attest that a
similar activity should have taken place in a relatively short time.
Associated with the highest number of sealings found in one room at
Kültepe, this assemblage attests to internal management of goods that
may be linked to transactions or exchange. In this case, however, this
would apply to goods other than tablets. The complete lack of
archaeological data as yet, impedes a deeper functional analysis of this
assemblage.

**[page 192 starts]**

## Conclusion and Perspectives

Making a review of sealing assemblages from Kültepe gives some hints
about their different usages. From secured contexts, we know that for
the lower city sealings marked with a cylinder seal seem to relate
almost exclusively to ‘archive management’, i.e. a tool to store groups
of tablets together [@Larsen2008a]. In contrast, clay sealings with
stamp seals from the lower city seem to have been attached to other
goods. In both cases the number of sealings is low, and duplicate
sealings with the same seal are not well attested. This short overview
reveals some questions that could shed new light on Kültepe, on the
management of goods in daily practices, and how these relate to other
contemporary cities.

A computation of the ratio between tablets or, better, the kind of
tablets with sealings could bring further hints at how these tablets
have been stored and what role the sealings played. Is there an archive
without sealings? Another crucial point that would shed light on the
usage of the seals, would be to check if it is possible to make a
classification on the point when the sealings have been broken (if at
all): during the abandonment of the building or perhaps some sealings
have been kept broken to record a past event?  Finally, a third approach
would need to compare the different sealing assemblages with other sites
from Central Anatolia. Veenhof [-@Veenhof1993; -@Veenhof2017a]
investigates closely the assemblages from Acemhöyük and their relations
to Kültepe. But how are the practices from Kültepe different from other
sites? A possibility for gaining knowledge on the shared prac- tices
would be to look for similarities of the sealings. This seems to be the
case for the mushroom type (see Fig. 10.19), but it is also probably the
case for the seal- ings used with a peg (see Figs 10.10–10.11,
10.20–10.21). There is also more work to accomplish on working out the
parallels in the icono­graphy of stamp seals, as shown with the 1977
citadel mound house and Boğazköy (see Figs 10.15–10.16). Could we add
other examples, such as from Kayalıpınar (see Fig. 10.22)? Is there any
distinct style, and not only a uniform ‘Anatolian’ style? At this point,
it should become evident that a closer look at the function of sealings,
especially anepi­graphic stamp sealings, has great potential to yield
new information on daily life during the Anatolian–Old Assyrian Period
and Kültepe as well as the broader central Anatolia region, which
remains under-investigated.


![](img/Bo64-632.jpg)

> *Caption Fig. 21*: A sealing from Boğazköy (Bo64-632), with almost
identical marks as Kt&nbsp;93/k&nbsp;809 and Kt&nbsp;93/k&nbsp;212 (see
Fig 10−11).  Photo Néhémie Strupler CC BY-SA 4.0, *Archiv der Boğazköy
Expedition*


![](img/kp2008-101_drawing.png)

> *Caption Fig. 22*:: A sealing from Kayalıpınar with
an iconography close to the iconography of sealings from the "1977
house" on Kültepe mound.  Redrawn after the photos in
@Muller-Karpe2009a, 195, fig. 9. CC BY Néhémie Strupler

**[page 193 starts]**


# *Acknowledgements* {-}

I would like to thank Cécile Michel, Fikri Kulakoğlu, and Guido Kryszat
for organizing the KIM meeting in 2019 and editing the proceedings, as
well as all the Kültepe archaeological team, who backed the conference
and granted us an ideal working environment. For this paper, I am
grateful to Cécile Michel and Andreas Schachner for authorizing the
reproduction of photos and for their feedback. I am also very grateful
to Andreas Schachner for providing the opportunity to study the Boğazköy
sealings in the Ankara Archaeological Museum and his generous sharing of
ideas in discussing the materials. Deniz Genceolu contributed with a
first editing of the English and Tim Barnwell increased the readability
of the text.

#  References {-}

